from peewee import *

db_proxy = Proxy()


class BaseModel(Model):
    class Meta:
        database = db_proxy


class Usuario(BaseModel):
    id = CharField(primary_key=True)
    nome = CharField()


class Loja(BaseModel):
    nome = CharField()


class Promocao(BaseModel):
    id = IntegerField(primary_key=True)
    usuario = ForeignKeyField(Usuario, related_name='promocoes')
    loja = ForeignKeyField(Loja, related_name='promocoes')
    titulo = TextField()
    link = TextField()
    link_imagem = TextField()
    n_comentarios = IntegerField()
    n_curtidas = IntegerField()
    preco = DecimalField(decimal_places=2, null=True)
    data_publicacao = DateTimeField()
