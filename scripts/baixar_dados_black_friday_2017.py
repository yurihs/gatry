#!/usr/bin/env python3
"""
Script para baixar dados da Black Friday 2017 para o banco de dados.
"""

import os
import gatry
from gatry.modelos import db_proxy
from peewee import *

# Cria o banco de dados
arquivo_database = 'gatryweb.db'
database = SqliteDatabase(arquivo_database, pragmas=(('foreign_keys', 'on'),))

# Inicializa o objeto que o aplicativo usa para interagir com o banco
db_proxy.initialize(database)

# Obtem as promoções do site do gatry
# O intervalo de IDs escolhido coincide com as postagens no período da BF 2017.

primeiro_id = 56003
ultimo_id = 56751
promos = gatry.scraper.promocoes_no_intervalo(primeiro_id, ultimo_id)

for promo in promos:
    # Salvar a promoção no banco
    promo.save(force_insert=True)
