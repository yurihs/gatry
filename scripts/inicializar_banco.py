#!/usr/bin/env python3
"""
Script para criar as tabelas do site
"""

import os
import gatry
from gatry.modelos import db_proxy
from peewee import *

# Cria o banco de dados
arquivo_database = 'gatryweb.db'
database = SqliteDatabase(arquivo_database, pragmas=(('foreign_keys', 'on'),))

# Inicializa o objeto que o aplicativo usa para interagir com o banco
db_proxy.initialize(database)

# Cria todas as tabelas
gatry.criar_tabelas(db_proxy)
