import decimal
import requests
from lxml import html
from collections import namedtuple
from datetime import datetime
from typing import Optional, Generator
from gatry.modelos import Promocao, Usuario, Loja

URL_GATRY = 'https://gatry.com/'


def detalhes_promocao(id_promocao: int, sessao: Optional[requests.Session] = None) -> Optional[Promocao]:
    """
    Obtém dados de uma promoção, a partir de sua página de detalhes.

    Exemplo:
    https://gatry.com/promocao/detalhes/39869

    Argumentos:
        id_promocao: Identificador numérico, maior que 0, da promoção em questão.
        sessao: Opcionalmente, usar uma sessão ao site já existente, para
                evitar ter que abrir uma nova conexão cada vez.

    Retorna:
        promocao: Se a promoção for encontrada.
        None: Se não for encontrada.

    """

    if not isinstance(id_promocao, int):
        raise ValueError('ID de promoção inválido')
    if id_promocao < 1:
        raise ValueError('ID de promoção inválido')

    url_promocao = '{0}/promocao/detalhes/{id}'.format(URL_GATRY, id=id_promocao)

    if sessao is None:
        # Criar uma nova sessão, caso uma não seja fornecida.
        sessao = requests.Session()

    resposta = sessao.get(url_promocao, allow_redirects=False)

    if resposta.status_code == 302:
        # Se uma promoção não existe, o site redireciona para a página principal
        # com o status code "302 Found".
        return None

    tree = html.fromstring(resposta.content)

    el_article = tree.cssselect('article.promocao')[0]

    el_usuario = el_article.cssselect('.usuario > a')[0]
    id_usuario = el_usuario.get('href').rsplit('/')[-1]
    nome_usuario = el_usuario.cssselect('img')[0].get('title')

    el_titulo = el_article.cssselect('h3[itemprop="name"] > a')[0]
    titulo = el_titulo.text
    link = el_titulo.get('href')

    el_imagem = el_article.cssselect('.imagem > a > img')[0]
    link_imagem = el_imagem.get('src')

    el_preco = el_article.cssselect('.preco > span[itemprop="price"]')[0]
    try:
        preco = decimal.Decimal(
            el_preco.text.replace('.', '').replace(',', '.')
        ).quantize(decimal.Decimal('0.00'))
    except decimal.InvalidOperation:
        preco = None

    el_comentarios = el_article.cssselect('.link-comentarios')[0]
    n_comentarios = int(el_comentarios.text.strip())

    el_curtidas = el_article.cssselect('.curtidas')[0]
    n_curtidas = int(el_curtidas.text_content().replace(' ', ''))

    el_loja = el_article.cssselect('.link_loja')[0]
    nome_loja = el_loja.getchildren()[0].tail

    el_data_publicacao = el_article.cssselect('.data_postado')[0]
    data_publicacao = datetime.strptime(
        el_data_publicacao.get('title'),
        '%d/%m/%Y às %H:%M'
    )

    usuario, _ = Usuario.get_or_create(id=id_usuario, nome=nome_usuario)
    loja, _ = Loja.get_or_create(nome=nome_loja)

    return Promocao(
        id=id_promocao,
        usuario=usuario,
        loja=loja,
        titulo=titulo,
        link=link,
        link_imagem=link_imagem,
        preco=preco,
        n_comentarios=n_comentarios,
        n_curtidas=n_curtidas,
        data_publicacao=data_publicacao
    )


def promocoes_no_intervalo(id_minimo: int, id_maximo: int,
                           sessao: Optional[requests.Session] = None) -> Generator[Promocao, None, None]:
    """
    Itera sobre as promoções existentes entre os IDs fornecidos.
    Ignora as não existentes.

    Argumentos:
        id_minimo: Inclusivo.
        id_maximo: Exclusivo.

    Gera:
        promocao
    """
    if not (isinstance(id_minimo, int) and isinstance(id_maximo, int)):
        raise ValueError('ID de promoção inválido fornecido.')
    if id_minimo < 1 or id_maximo < 1:
        raise ValueError('ID de promoção inválido fornecido.')

    if sessao is None:
        sessao = requests.Session()

    for id_promocao in range(id_minimo, id_maximo):
        promocao = detalhes_promocao(id_promocao, sessao=sessao)
        if promocao is not None:
            yield promocao
