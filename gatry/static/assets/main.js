document.addEventListener('DOMContentLoaded', function (){
    fetch('/stats/preco_cumulativo')
      .then((response) => response.json())
      .then(mostrar_grafico_preco_cumulativo);

    fetch('/stats/promocoes_por_loja')
      .then((response) => response.json())
      .then(mostrar_grafico_promocoes_por_loja);

    fetch('/stats/palavras_mais_usadas')
      .then((response) => response.json())
      .then(mostrar_grafico_palavras_mais_usadas);

}, false);


function mostrar_grafico_preco_cumulativo(dados) {
  serie = {
      name: 'Preço cumulativo',
      data: []
  }
  serie.data = dados.map(function (dado){
      return {
        x: new Date(dado[0] * 1000),
        y: dado[1]
      };
  })
  opcoes = {
    axisX: {
      type: Chartist.FixedScaleAxis,
      divisor: 6,
      low: moment('2017-11-23 22:00:00').unix() * 1000,
      high: moment('2017-11-25 00:00:00').unix() * 1000,
      labelInterpolationFnc: function(value) {
        return moment(value).format('MMM D HH:mm');
      }
    },
    axisY: {
      labelInterpolationFnc: function(value, index) {
        return 'R$' + value.toLocaleString();
      },
      labelOffset: {x: -5, y: 0}
    },
    chartPadding: {
        top: 20,
        right: 0,
        bottom: 0,
        left: 40
    },
    lineSmooth: 0,
    showPoint: false
  };
  new Chartist.Line('#grafico-preco-cumulativo', {series: [serie]}, opcoes);
}

function mostrar_grafico_promocoes_por_loja(dados) {
  labels = [];
  serie = {
      name: 'Promoções por loja',
      data: []
  }
  dados.forEach(function (dado){
    labels.push(dado.nome);
    serie.data.push(dado.n_promocoes);
  })
  opcoes = {
    axisY: {
       onlyInteger: true
    },
    chartPadding: {
        top: 0,
        right: 0,
        bottom: 40,
        left: 0
    }
  };
  new Chartist.Bar('#grafico-promocoes-por-loja', {labels: labels, series: [serie]}, opcoes);
}

function mostrar_grafico_palavras_mais_usadas(dados) {
  labels = [];
  serie = {
      name: 'Palavras mais usadas',
      data: []
  }
  dados.forEach(function (dado){
    labels.push(dado.palavra);
    serie.data.push(dado.n_usos);
  })
  opcoes = {
    axisY: {
       onlyInteger: true
    },
    chartPadding: {
        top: 0,
        right: 0,
        bottom: 40,
        left: 0
    }
  };
  new Chartist.Bar('#grafico-palavras-mais-usadas', {labels: labels, series: [serie]}, opcoes);
}
