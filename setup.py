from setuptools import setup

version = "0.0.1"

requirements = [
    "requests",
    "lxml",
    "cssselect",
    "peewee",
    "flask",
    "gunicorn",
    "psycopg2",
    "flask-peewee",
    "Babel"
]

setup(
    name="gatry",
    version=version,
    description="Scrape gatry.com for sales",
    author="yurihs",
    packages=["gatry"],
    packagedir={"gatry": "gatry"},
    include_package_data=True,
    install_requires=requirements,
    setup_requires=['pytest-runner'],
    tests_require=['pytest'],
    license="MIT",
    zip_safe=False,
    classifiers=[
        'Programming Language :: Python :: 3'
    ]
)
