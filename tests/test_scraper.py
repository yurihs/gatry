import itertools
import pytest
import decimal
from datetime import datetime
from gatry import scraper


@pytest.mark.usefixtures('db_teste')
class TestDetalhesPromocao():

    def test_promocao_valida(self):
            id_promocao_valida = 3333

            promo = scraper.detalhes_promocao(id_promocao_valida)
            promo.save(force_insert=True)

            assert promo.id == id_promocao_valida
            assert promo.titulo == 'Kindle Geração 7 com Wi-Fi e Tela Touch de 6" AMAZON '
            assert promo.usuario.id == 'cAcdqU75'
            assert promo.usuario.nome == 'ThumbDown'
            assert promo.loja.nome == 'Megamamute'
            assert promo.link == 'http://oferta.vc/elCt'
            assert promo.n_comentarios == 5
            assert promo.n_curtidas == 5
            assert promo.preco == decimal.Decimal('190.43')
            assert promo.data_publicacao == datetime(2015, 8, 1, 9, 29)

    def test_promocao_inexistente(self):
            id_promocao_inexistente = 25

            promo = scraper.detalhes_promocao(id_promocao_inexistente)

            assert promo is None

    def test_id_invalido(self):
            with pytest.raises(ValueError):
                promo = scraper.detalhes_promocao(0)
                promo = scraper.detalhes_promocao('invalido')
                promo = scraper.detalhes_promocao('123')
                promo = scraper.detalhes_promocao(None)


@pytest.mark.usefixtures('db_teste')
class TestInvervaloPromocoes():

    def test_intervalo(self):
            minimo = 200
            quantidade = 4
            maximo = minimo + quantidade
            nomes_esperados = [
                'Caixa de som Bluetooth, 10 W x 2, air play, sinal digital JBL - HKSOUNFLY ',
                'Livro - Bilionários por Acaso: A Criação do Facebook',
                'Sound Bar LG LA550H 320W Subwoofer Wireless Função Sound Sync Wireless',
                'Geladeira / Refrigerador Brastemp Ative 2 Portas BRM48 Frost Free 403L Inox'
            ]

            for nome_esperado, promo in itertools.zip_longest(nomes_esperados,
                                                              scraper.promocoes_no_intervalo(minimo, maximo)):
                promo.save(force_insert=True)
                assert promo.titulo == nome_esperado

    def test_intervalo_inexistente(self):
            minimo = 25
            maximo = minimo + 1

            promos = [x for x in scraper.promocoes_no_intervalo(minimo, maximo)]

            assert len(promos) == 0

    def test_intervalo_invalido(self):
        intervalos_invalidos = [
            ('a', 0),
            (0, 'b'),
            ('a', 'b'),
            (10, 'invalido'),
            ('invalido', 10),
            (10, None),
            (None, 10),
            (None, None)
        ]

        with pytest.raises(ValueError):
            for minimo, maximo in intervalos_invalidos:
                promos = [x for x in scraper.promocoes_no_intervalo(minimo, maximo)]
