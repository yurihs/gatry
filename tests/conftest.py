# content of conftest.py
import pytest
from peewee import SqliteDatabase
from gatry.modelos import Promocao, Loja, Usuario


@pytest.fixture(scope="class")
def db_teste():
    test_db = SqliteDatabase(':memory:')
    with test_db.bind_ctx((Promocao, Loja, Usuario)):
        test_db.create_tables((Promocao, Loja, Usuario))
        yield
        test_db.drop_tables((Promocao, Loja, Usuario))
