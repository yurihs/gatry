# Gatry: Promoções da Black Friday 2017

Dados de promoções postadas no [**Gatry.com**](https://gatry.com).

Processados em [**Python**]("https://www.python.org/) e armazenados em um
banco de dados [**PostgreSQL**](https://postgresql.org/).

Modelos disponibilizados em uma API baseada em [**REST**](https://pt.wikipedia.org/wiki/REST)
e visualizados com [**Chartist.js**](https://gionkunz.github.io/chartist-js/).


## >> Demonstração <<

**Uma _demo_ do projeto está disponível em [bsi-prog2-gatry.herokuapp.com](https://bsi-prog2-gatry.herokuapp.com/).**

## Estrutura do projeto

Os cinco principais arquivos do projeto são:

- ``modelos.py``: Define como serão armazenadas as informações: Promoções,
usuários, e lojas.

- ``scraper.py``: Funções para extraír os dados do
[Gatry.com](https://gatry.com) _(scraping)_. Ou seja, para preencher o banco de
dados com as promoções, os usuários que as postaram, e as lojas.

- ``web.py``: Define a aplicação _web_ que apresenta as informações em gráficos
e listagens. Serve arquivos estáticos (HTML, CSS, JavaScript), e os _endpoints_
da API.

- ``templates/index.html``: É renderizado pelo aplicativo _web_ para mostrar as
estatísticas e listagens.

- ``static/assets/main.js``: Configura os gráficos de estatística, fazendo
consultas aos _endpoints_ da API.

## Principais bibliotecas usadas

**Python**

- [**requests**](https://python-requests.org/): Para fazer pedidos e manter conexões com o Gatry.com, no
processo de _scraping_.

- [**lxml**](http://lxml.de/): Para extrair os dados dos elementos do HTML, no processo
de _scraping_.

- [**peewee**](https://peewee-orm.com/): ORM para abstrair o uso do banco de dados. Usada para modelar e
guardar os dados resultantes do _scraping_, e realizar consultas sobre eles
posteriormente.

- [**Flask**](http://flask.pocoo.org/): _Microframework_ para web. Responde às requisições HTTP e gerencia
os contextos de cada pedido.

- [**Gunicorn**](https://gunicorn.org): Servidor web eficiente compatível com WSGI, e consequentemente,
com o Flask e outras _frameworks web_ similares.

---

## Instruções de instalação

Caso queira executar o projeto localmente, você deverá clonar esse repositório
em seu computador, de qualquer um desses repositórios:

- ```git clone ssh://git@git.fabricadesoftware.ifc.edu.br:1022/yurihs/gatry.git```
- ```git clone https://gitlab.com/yurihs/gatry.git```

```cd gatry```

Após clonar o reposítório, e ir até a sua pasta, crie um _virtualenv_.
**O Python 3 é requerido.**

```virtualenv -p python3 venv```

```source venv/bin/activate```

Instale as dependências do projeto com o pip.

```pip install . ```

Agora você pode rodar o servidor com o comando:

```gunicorn gatry.web:app```

Mas ainda não existem tabelas no banco de dados, por isso ocorrerão erros.
Vamos resolver isso com executando o script de criação de tabelas:

```python scripts/inicializar_banco.py```

As tabelas estão criadas, mas como estão vazias, alguns erros ainda acontecem.
Use esse script para baixar as promoções do período da Black Friday 2017 do site
Gatry. Note que esse passo pode demorar alguns minutos (3 a 10), pois
são feitos cerca de 300 pedidos ao site.

```python scripts/baixar_dados_black_friday_2017.py```

Pronto, quando você rodar o servidor novamente, você poderá abrir o seu
navegador em ```http://localhost:8000``` para ver o site.
