import peewee
from gatry import scraper, modelos


def criar_tabelas(db: peewee.Database):
    db.create_tables([
        modelos.Loja, modelos.Usuario, modelos.Promocao
    ])


def criar_banco_temporario():
    database = SqliteDatabase('gatryweb.db', pragmas=(('foreign_keys', 'on'),))
    modelos.db_proxy.initialize(database)
    criar_tabelas(modelos.db_proxy)
    return modelos.db_proxy
